# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: ISC_DOF.py 10120 2015-03-18 18:21:10Z sheila.dwyer@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/isc/h1/guardian/ISC_DOF.py $

from guardian import GuardState, GuardStateDecorator
from guardian.ligopath import userapps_path
from guardian import NodeManager
from subprocess import check_output
import cdsutils
import time
from math import sqrt, pow

##################################################
import lscparams as lscparams

from ISC_library import *
from ISC_GEN_STATES import *


##################################################
ca_monitor = False
ca_monitor_notify = False
##################################################

#TODO: DRMI lock threshold input power scaling
#TODO: Power scaling for other thresholds
#TODO: DC readout transition
#TODO: Turn on WFS once at zero carm offset
#TODO: Find carrier resonance in OMC length


###################################################


##################################################
## filter names
##################################################
#from ezca import Ezca as ezca_dummy
#prcl_filt = ezca.get_LIGOFilter('LSC-PRCL')
#mich_filt = ezca.get_LIGOFilter('LSC-MICH')
#srcl_filt = ezca_dummy().get_LIGOFilter('LSC-SRCL')
#arm_filt = ezca.get_LIGOFilter('LSC-%sARM'%arm)
#prm_m2_filt = ezca.get_LIGOFilter('SUS-PRM_M2_LOCK_L')
#srm_m2_filt = ezca.get_LIGOFilter('SUS-SRM_M2_LOCK_L')


##################################################
# NODES
##################################################

nodes = NodeManager(['ALIGN_IFO'])

##################################################
# STATES
##################################################

# FIXME: this needs to check state and move to the right place on the
# graph
class INIT(GuardState):
    request = True
    def main(self):
         print 'hello'
         log("initializing subordinate nodes...")
         nodes.set_managed()
    def run(self):
         return True



# reset everything to the good values for acquistion.  These values
# are stored in the down script.
class DOWN(GuardState):
    goto = True
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        #turn off ASC
        ezca.get_LIGOFilter('ASC-INP1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-MICH_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-MICH_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC3_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC4_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC3_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-DC4_Y').switch_off('INPUT')

        # clear histories
        ezca['SUS-PRM_M1_LOCK_P_RSET'] = 2
        ezca['SUS-PRM_M1_LOCK_Y_RSET'] = 2
        ezca['ASC-PRC1_P_RSET'] = 2
        ezca['ASC-PRC1_Y_RSET'] = 2


		# turn off SRCL
	#	srcl_filt().ramp_gain(0, ramp_time=3, wait=False)

        #nodes['ALIGN_IFO'] = 'DOWN'

    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        return True

##################################################
# LOCK DOF uing ISC configs
##################################################
def gen_LOCK_DOF():
    class LOCK_DOF(GuardState): 
        request = False
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        @assert_mc_locked
        def main(self):
            nodes['ALIGN_IFO'] = '%s_LOCKED'%self.dof

        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        @assert_mc_locked
        def run(self):
            if nodes.arrived:
                return True
            else:
                notify('WAITING for LSC_CONFIG')
    return LOCK_DOF

LOCK_XARM_IR = gen_LOCK_DOF()
LOCK_XARM_IR.dof = 'XARM_IR'
LOCK_PRX = gen_LOCK_DOF()
LOCK_PRX.dof = 'PRX'
LOCK_SRY = gen_LOCK_DOF()
LOCK_SRY.dof = 'SRY'


#############################
## states to take care of WFS centering
###################################

REFL_WFS_CENTERING_XARM = gen_REFL_WFS_CENTERING('XARM')
REFL_WFS_CENTERING_PRX = gen_REFL_WFS_CENTERING('PRX')
REFL_WFS_CENTERING_SRY = gen_REFL_WFS_CENTERING('SRY')

#######################################
## Align PRM using refl WFS and PRX
class PRM_ALIGN(GuardState):
    request = True
    @assert_mc_locked
    @assert_prx_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        # turn on PRM feedback to M3
        ezca['SUS-PRM_M3_LOCK_OUTSW_P'] = 1 
        ezca['SUS-PRM_M3_LOCK_OUTSW_Y'] = 1 
        # turn off PRM feedback to M2
        ezca['SUS-PRM_M2_LOCK_OUTSW_P'] = 0 
        ezca['SUS-PRM_M2_LOCK_OUTSW_Y'] = 0 
        # turn on PRM feedback to M1
        ezca.switch('SUS-PRM_M1_LOCK_P', 'INPUT', 'OUTPUT', 'ON') 
        ezca.switch('SUS-PRM_M1_LOCK_Y', 'INPUT', 'OUTPUT', 'ON')
        # Reset the PRC/SRC WFS filters
        ezca['ASC-PRC1_P_RSET'] = 2 
        ezca['ASC-PRC1_Y_RSET'] = 2 
        #REFL B 9 I to PRC1  
        for jj in range(1,28):
            ezca['ASC-INMATRIX_P_3_%d'%jj] = 0
            ezca['ASC-INMATRIX_Y_3_%d'%jj] = 0
        ezca['ASC-INMATRIX_P_3_13'] = 1
        ezca['ASC-INMATRIX_Y_3_13'] = 1
        # control filters
        ezca.get_LIGOFilter('ASC-PRC1_P').only_on('FM2', 'FM3','FM5', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-PRC1_Y').only_on('FM2', 'FM3', 'FM5','OUTPUT', 'DECIMATION')
        ezca['ASC-PRC1_P_GAIN'] = -300 # increased from -40 March 14 2015 SED
        ezca['ASC-PRC1_Y_GAIN'] = 40  #gains increase SB
        #output matrix
        ezca['ASC-OUTMATRIX_P_1_3'] = 1
        ezca['ASC-OUTMATRIX_Y_1_3'] = 1 
        ezca['ASC-OUTMATRIX_P_2_3'] = 0
        ezca['ASC-OUTMATRIX_Y_2_3'] = 0 
        ezca['ASC-OUTMATRIX_P_9_3'] = 0
        ezca['ASC-OUTMATRIX_Y_9_3'] = 0 
        ezca['ASC-OUTMATRIX_P_10_3'] = 0
        ezca['ASC-OUTMATRIX_Y_10_3'] = 0 
        ezca['ASC-OUTMATRIX_P_15_3'] = 0
        ezca['ASC-OUTMATRIX_Y_15_3'] = 0 
        #turn on servos
        ezca.get_LIGOFilter('ASC-PRC1_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-PRC1_Y').switch_on('INPUT')



#######################################
## Align SRM using refl WFS and SRX

class SRC_ALIGN(GuardState):
    request = True
    @assert_mc_locked
    @assert_sry_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        # turn off SRM feedback to M2
        ezca['SUS-SRM_M2_LOCK_OUTSW_P'] = 0 
        ezca['SUS-SRM_M2_LOCK_OUTSW_Y'] = 0 
        # turn on SRM feedback to M1
        ezca.switch('SUS-SRM_M1_LOCK_P', 'INPUT', 'OUTPUT', 'ON') 
        ezca.switch('SUS-SRM_M1_LOCK_Y', 'INPUT', 'OUTPUT', 'ON')
        # turn on SR2 feedback to M1
        ezca.switch('SUS-SR2_M1_LOCK_P', 'INPUT', 'OUTPUT', 'ON') 
        ezca.switch('SUS-SR2_M1_LOCK_Y', 'INPUT', 'OUTPUT', 'ON')
        # Reset the SRC WFS filters
        ezca['ASC-SRC1_P_RSET'] = 2 
        ezca['ASC-SRC1_Y_RSET'] = 2 
        ezca['ASC-SRC2_P_RSET'] = 2 
        ezca['ASC-SRC2_Y_RSET'] = 2 
        # input matrix
        clear_asc_input_matrix()
        ezca['ASC-INMATRIX_P_12_17'] = 1
        ezca['ASC-INMATRIX_Y_12_17'] = 1
        ezca['ASC-INMATRIX_P_13_18'] = 1
        ezca['ASC-INMATRIX_Y_13_18'] = 1
        #REFL 9 I to SRC1        
        ezca['ASC-INMATRIX_P_6_9'] = 1
        ezca['ASC-INMATRIX_Y_6_9'] = 1
        #AS_C 9 to SRC2        
        ezca['ASC-INMATRIX_P_7_27'] = 1
        ezca['ASC-INMATRIX_Y_7_27'] = 1
        # control filters
        ezca['ASC-SRC1_P_LIMIT'] = 100000
        ezca['ASC-SRC1_Y_LIMIT'] = 100000
        ezca['ASC-SRC2_P_LIMIT'] = 100000
        ezca['ASC-SRC2_Y_LIMIT'] = 100000
        ezca.get_LIGOFilter('ASC-SRC1_P').only_on('FM5','FM4', 'OUTPUT','LIMIT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC1_Y').only_on('FM5', 'FM4','OUTPUT','LIMIT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC2_P').only_on('FM4','FM5', 'OUTPUT','LIMIT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-SRC2_Y').only_on('FM4','FM5','OUTPUT','LIMIT', 'DECIMATION')
        ezca['ASC-SRC1_P_GAIN'] = 2
        ezca['ASC-SRC1_Y_GAIN'] = 2
        ezca['ASC-SRC2_P_GAIN'] = 100
        ezca['ASC-SRC2_Y_GAIN'] = 100
        #output matrix
        ezca['ASC-OUTMATRIX_P_9_6'] = 1
        ezca['ASC-OUTMATRIX_Y_9_6'] = 1 
        ezca['ASC-OUTMATRIX_P_11_6'] = 0
        ezca['ASC-OUTMATRIX_Y_11_6'] = 0 
        ezca['ASC-OUTMATRIX_P_10_7'] = 1
        ezca['ASC-OUTMATRIX_Y_10_7'] = 1 
        ezca['ASC-OUTMATRIX_P_9_7'] = -7.08
        ezca['ASC-OUTMATRIX_Y_9_7'] = 7.12
        # set up sus
        ezca['SUS-SRM_M3_LOCK_OUTSW_P'] = 0
        ezca['SUS-SRM_M3_LOCK_OUTSW_Y'] = 0
        ezca['SUS-SR2_M3_LOCK_OUTSW_P'] = 0
        ezca['SUS-SR2_M3_LOCK_OUTSW_Y'] = 0
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_P').only_on('FM1', 'FM9', 'INPUT', 'OUTPUT', 'DECIMATION', 'LIMIT')
        ezca.get_LIGOFilter('SUS-SRM_M1_LOCK_Y').only_on('FM1', 'FM9', 'INPUT', 'OUTPUT', 'DECIMATION', 'LIMIT')
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_P').only_on('FM2', 'FM3', 'INPUT', 'OUTPUT', 'DECIMATION', 'LIMIT')
        ezca.get_LIGOFilter('SUS-SR2_M1_LOCK_Y').only_on('FM2', 'FM3', 'INPUT', 'OUTPUT', 'DECIMATION', 'LIMIT')
        time.sleep(1)
        ezca.get_LIGOFilter('ASC-SRC1_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-SRC1_Y').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-SRC2_Y').switch_on('INPUT')


#######################################
## Align input pointing  IM4 + pR2 to to X arm using refl WFS
class SET_INPUT_ALIGN(GuardState):
    request = False
    @assert_mc_locked
    @assert_xarm_IR_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        # set up PR2 (this is different than PR2 ASC set up)
        ezca.get_LIGOFilter('SUS-PR2_M1_LOCK_P').only_on('DECIMATION','OUTPUT','FM1','FM2','INPUT') 
        ezca.get_LIGOFilter('SUS-PR2_M1_LOCK_Y').only_on('DECIMATION','OUTPUT','FM1','FM2','INPUT') 
        ezca['SUS-PR2_M3_LOCK_OUTSW_P'] = 0 
        ezca['SUS-PR2_M3_LOCK_OUTSW_Y'] = 0 
        # input matrix
        for jj in range(1, 28):
            ezca['ASC-INMATRIX_P_1_%d'%jj] = 0 
            ezca['ASC-INMATRIX_P_2_%d'%jj] = 0 
            ezca['ASC-INMATRIX_Y_1_%d'%jj] = 0 
            ezca['ASC-INMATRIX_Y_2_%d'%jj] = 0
        ezca['ASC-INMATRIX_P_1_9'] = 1
        ezca['ASC-INMATRIX_Y_1_9'] = 1
        ezca['ASC-INMATRIX_P_2_13'] = 1
        ezca['ASC-INMATRIX_Y_2_13'] = 1
        # control filters
        ezca.get_LIGOFilter('ASC-INP1_P').only_on('FM5','OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-INP1_Y').only_on('FM5', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-INP2_P').only_on('FM5', 'OUTPUT', 'DECIMATION')
        ezca.get_LIGOFilter('ASC-INP2_Y').only_on('FM5', 'OUTPUT', 'DECIMATION')
        ezca['ASC-INP1_P_GAIN'] = 50
        ezca['ASC-INP1_Y_GAIN'] = -50
        ezca['ASC-INP2_P_GAIN'] = -0.7
        ezca['ASC-INP2_Y_GAIN'] = -0.7
        #output matrix
        for jj in range(1, 25):
            ezca['ASC-OUTMATRIX_P_2_%d'%jj] = 0
            ezca['ASC-OUTMATRIX_P_15_%d'%jj] = 0
            ezca['ASC-OUTMATRIX_Y_2_%d'%jj] = 0
            ezca['ASC-OUTMATRIX_Y_15_%d'%jj] = 0
        ezca['ASC-OUTMATRIX_P_2_2'] = 1
        ezca['ASC-OUTMATRIX_P_15_2'] = -125
        ezca['ASC-OUTMATRIX_P_15_1'] = 1
        ezca['ASC-OUTMATRIX_Y_2_2'] = 1
        ezca['ASC-OUTMATRIX_Y_15_2'] = 40
        ezca['ASC-OUTMATRIX_Y_15_1'] = 1
        
        
    @assert_mc_locked
    @assert_xarm_IR_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def run(self):
        if not ASC_DC_centering_servos_OK():
            notify('REFL WFS DC centering')
            return 'REFL_WFS_CENTERING_XARM'
        else:
            return True

class INPUT_ALIGN(GuardState):
    request = True
    @assert_mc_locked
    @assert_xarm_IR_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        if not ASC_DC_centering_servos_OK():
            notify('REFL WFS DC centering')
            ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
            return 'SET_INPUT_ALIGN'
        ezca.get_LIGOFilter('ASC-INP1_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-INP1_Y').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_P').switch_on('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_Y').switch_on('INPUT')  

    @assert_mc_locked
    @assert_xarm_IR_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
	def run(self):
        if not ASC_DC_centering_servos_OK():
            notify('REFL WFS DC centering')
            ezca.get_LIGOFilter('ASC-DC1_P').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC1_Y').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC2_P').switch_off('INPUT')
            ezca.get_LIGOFilter('ASC-DC2_Y').switch_off('INPUT')
			return 'SET_INPUT_ALIGN'
		else:	
			return True

'''class OFFLOAD_INPUT_ALIGN(GuardState):
    request = False
    @assert_mc_locked
    @assert_xarm_IR_locked
    @get_subordinate_watchdog_check_decorator(nodes)
    @nodes.checker()
    def main(self):
        #offload
        offload('IM4', 'M1')
        offload('PR2', 'M1')
        # turn off the loops
        ezca.get_LIGOFilter('ASC-INP1_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP1_Y').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_P').switch_off('INPUT')
        ezca.get_LIGOFilter('ASC-INP2_Y').switch_off('INPUT')
        #clear history
        ezca['ASC-INP1_P_RSET'] = 2
        ezca['ASC-INP1_Y_RSET'] = 2
        ezca['ASC-INP2_P_RSET'] = 2
        ezca['ASC-INP2_Y_RSET'] = 2
        notify('Save alignments of PR2 +IM4')
        return True'''

def gen_ALIGN_SLIDER_SERVO(dof):
    class ALIGN_SLIDER_SERVO(GuardState):
        request = True
        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def main(self):
            # time constant for the running average of the error signals
            self.T = 2*numpy.pi*10 
            # initial error values (used for termination condition)
            self.average_err = numpy.zeros(len(self.errors))
            for i in range(len(self.errors)):
                # get error signal value
                    errsig = ezca[self.errors[i]]
                    self.average_err[i] = errsig
            # compute the maximum of error signal absolute values
            self.max_err = max(abs(self.average_err))
            # set ramp times
            for i in range(len(self.actuators)):
                ezca[self.actuators[i] +'_TRAMP'] = 1                

        @assert_mc_locked
        @assert_dof_locked_gen(dof)
        @get_subordinate_watchdog_check_decorator(nodes)
        @nodes.checker()
        def run(self):
            t0 = time.time()
            # compute new maximum value of error signal
            self.max_err = max(abs(self.average_err))
            log(self.max_err)
            # wait a little bit
            time.sleep(0.01)
            # Main loop, continue until error signals are all smaller than threshold. Stop if power drops below threshold
            if self.max_err > self.err_threshold and ezca[self.powersig] > self.threshold:
                # compute the time since the last actuation, to maintain uniform gain
                dt = time.time() - t0
                # loop over all error signals
                for i in range(len(self.errors)):
                    # get error signal value
                    errsig = ezca[self.errors[i]]
                    # compute new position and apply it
                    ezca[self.actuators[i] + '_OFFSET'] = ezca[self.actuators[i] + '_OFFSET'] + self.gains[i] * dt * errsig
                    # running average on error signal value
                    self.average_err[i] = (1 - 1./self.T) * self.average_err[i] + 1./self.T * ezca[self.errors[i]]*errsig
            else:
                return True
    return ALIGN_SLIDER_SERVO



OFFLOAD_INPUT_ALIGN = gen_OFFLOAD_ALIGNMENT('XARM', 10)
OFFLOAD_INPUT_ALIGN.stage = ['M1','M1']
OFFLOAD_INPUT_ALIGN.optics = ['IM4', 'PR2']

OFFLOAD_SRC_ALIGNMENT = gen_OFFLOAD_ALIGNMENT('SRY', 10)
OFFLOAD_SRC_ALIGNMENT.optics = ['SRM','SR2']
OFFLOAD_SRC_ALIGNMENT.stage = ['M1','M1']

OFFLOAD_PRM_ALIGNMENT = gen_OFFLOAD_ALIGNMENT('PRX', 10)
OFFLOAD_PRM_ALIGNMENT.optics = ['PRM']
OFFLOAD_PRM_ALIGNMENT.stage = ['M1']

##################################################
##################################################

edges = [
# PRX edges:
    ('DOWN','LOCK_PRX'),
    ('LOCK_PRX', 'REFL_WFS_CENTERING_PRX'), 
    ('REFL_WFS_CENTERING_PRX', 'PRM_ALIGN'),
    ('PRM_ALIGN', 'OFFLOAD_PRM_ALIGNMENT'), 
# SRY edges:
    ('DOWN', 'LOCK_SRY'),
    ('LOCK_SRY', 'REFL_WFS_CENTERING_SRY'), 
    ('REFL_WFS_CENTERING_SRY','SRC_ALIGN'),
    ('SRC_ALIGN', 'OFFLOAD_SRC_ALIGNMENT'),
#XARM IR edges:
    ('DOWN', 'LOCK_XARM_IR'),
    ('LOCK_XARM_IR', 'REFL_WFS_CENTERING_XARM'),
    ('REFL_WFS_CENTERING_XARM', 'SET_INPUT_ALIGN'), 
    ('SET_INPUT_ALIGN', 'INPUT_ALIGN'),
    ('INPUT_ALIGN', 'OFFLOAD_INPUT_ALIGN'),
    ('OFFLOAD_INPUT_ALIGN', 'LOCK_XARM_IR')
    ]

